activation
activate
	| args |
	
	self activateHelp
		ifTrue: [  ^ self ].
		
	(args := self arguments) 
		ifEmpty: [ ^ self help ].
		
	SmalltalkImage current 
		at: (args first asSymbol)
		ifPresent: [ :processor | 
			processor process: args last.
			self quit ].
	
	self exitFailure: 'Error: Unable to locate #', args first
	